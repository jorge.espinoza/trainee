﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;
using System.Globalization;

namespace Addon_Super
{
    [FormAttribute("UDO_FT_SEI_UDOMER")]
    class UDOForm1 : UDOFormBase
    {
        public UDOForm1()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("0_U_G").Specific));
            this.Matrix0.ValidateAfter += new SAPbouiCOM._IMatrixEvents_ValidateAfterEventHandler(this.Matrix0_ValidateAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.Matrix Matrix0;

        private void Matrix0_ValidateAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try
            {
                if (pVal.ItemChanged &&(pVal.ColUID.Equals("C_0_2") || pVal.ColUID.Equals("C_0_3")))
                {
                    SAPbouiCOM.Cell oCell2 = ((SAPbouiCOM.Matrix)sboObject).Columns.Item("C_0_2").Cells.Item(pVal.Row);
                    SAPbouiCOM.Cell oCell3 = ((SAPbouiCOM.Matrix)sboObject).Columns.Item("C_0_3").Cells.Item(pVal.Row);
                    
                    Double cant2 = -1; //cantidad
                    Double cant3 = -1; //precio unitario
                    Double total = 0;
                    Double.TryParse(((SAPbouiCOM.IEditText)oCell2.Specific).Value, NumberStyles.Any, CultureInfo.InvariantCulture, out cant2);
                    Double.TryParse(((SAPbouiCOM.IEditText)oCell3.Specific).Value, NumberStyles.Any, CultureInfo.InvariantCulture, out cant3);
                    if (cant2 > -1 && cant3 > -1)
                    {
                        total = cant2 * cant3;
                        SAPbouiCOM.Cell oCell4 = ((SAPbouiCOM.Matrix)sboObject).Columns.Item("C_0_4").Cells.Item(pVal.Row);
                        String totals = total.ToString();
                        ((SAPbouiCOM.IEditText)oCell4.Specific).Value = totals;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        private void OnCustomInitialize()
        {

        }
    }
}
